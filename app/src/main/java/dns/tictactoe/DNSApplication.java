package dns.tictactoe;

import android.app.Application;

import com.google.android.gms.ads.MobileAds;

/**
 * Created by nayan on 10/31/16.
 */

public class DNSApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        MobileAds.initialize(this,"ca-app-pub-2747365560958881~3872483454");
    }
}
