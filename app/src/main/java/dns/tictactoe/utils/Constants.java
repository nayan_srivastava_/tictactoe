package dns.tictactoe.utils;

/**
 * Created by nayan on 10/31/16.
 */

public class Constants {
    public static final String[] USER_WIN_MESSAGE={
            "Great! Well deserved victory. Play again",
            "Smashed! Congrats, play again",
            "Woah! Champion, You won. Play again",
            "Terrific! Play again"
    };

    public static final String [] BOT_WIN_MESSAGES={
            "Great contest! You lost, try again",
            "Hard luck! You lost, fight again",
            "Everybody loses! Don't mind try again"
    };


}
