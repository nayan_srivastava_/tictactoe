package dns.tictactoe.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by nayan on 10/31/16.
 */

public final class PrefUtil {
    private static final String FILE_NAME = "ttt";

    public static SharedPreferences getPref(Context context) {
        return context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }

    public static void putInt(String key, int val, Context context) {
        SharedPreferences.Editor editor = getPref(context).edit();
        editor.putInt(key, val).apply();
    }

    public static int getInt(String key, Context context) {
        return getPref(context).getInt(key, 0);
    }
}
