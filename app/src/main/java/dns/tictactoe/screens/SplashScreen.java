package dns.tictactoe.screens;

import android.content.Intent;
import android.os.Bundle;

import com.daimajia.androidanimations.library.Techniques;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.viksaa.sssplash.lib.activity.AwesomeSplash;
import com.viksaa.sssplash.lib.cnst.Flags;
import com.viksaa.sssplash.lib.model.ConfigSplash;

import java.util.Calendar;

import dns.tictactoe.R;

/**
 * Created by nayan on 10/31/16.
 */

public class SplashScreen extends AwesomeSplash {

    boolean movedToPlayScreen = false;

    @Override
    public void initSplash(ConfigSplash configSplash) {
          /* you don't have to override every property */

        //Customize Circular Reveal
        configSplash.setBackgroundColor(R.color.colorPrimary); //any color you want form colors.xml
        configSplash.setAnimCircularRevealDuration(2000); //int ms
        configSplash.setRevealFlagX(Flags.REVEAL_RIGHT);  //or Flags.REVEAL_LEFT
        configSplash.setRevealFlagY(Flags.REVEAL_BOTTOM); //or Flags.REVEAL_TOP

        //Choose LOGO OR PATH; if you don't provide String value for path it's logo by default

        //Customize Logo
        configSplash.setLogoSplash(R.mipmap.ic_launcher); //or any other drawable
        configSplash.setAnimLogoSplashDuration(4000); //int ms
        configSplash.setAnimLogoSplashTechnique(Techniques.Bounce); //choose one form Techniques (ref: https://github.com/daimajia/AndroidViewAnimations)


        //Customize Path
        // configSplash.setPathSplash(Constants.DROID_LOGO); //set path String
        configSplash.setOriginalHeight(600); //in relation to your svg (path) resource
        configSplash.setOriginalWidth(600); //in relation to your svg (path) resource
        configSplash.setAnimPathStrokeDrawingDuration(3000);
        configSplash.setPathSplashStrokeSize(3); //I advise value be <5
        configSplash.setPathSplashStrokeColor(R.color.splashFill); //any color you want form colors.xml
        configSplash.setAnimPathFillingDuration(3000);
        configSplash.setPathSplashFillColor(R.color.splashFill); //path object filling color


        //Customize Title
        configSplash.setTitleSplash("Let's Play");
        configSplash.setTitleTextColor(android.R.color.white);
        configSplash.setTitleTextSize(30f); //float value
        configSplash.setAnimTitleDuration(3000);
        configSplash.setAnimTitleTechnique(Techniques.FlipInX);

        Thread navThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(9500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                navToPlayScreen();
            }
        });
        navThread.start();
    }

    private void navToPlayScreen() {
        Intent intent = new Intent(this, PlayScreen.class);
        movedToPlayScreen = true;
        startActivity(intent);
        finish();
    }

    @Override
    public void setFont(String font) {
        // do nothing
    }

    @Override
    public void animationsFinished() {

    }

    private void sendAppClose() {
        Bundle bundle = new Bundle();
        bundle.putString("at", Calendar.getInstance().getTime().toString());
        FirebaseAnalytics.getInstance(this).logEvent("Closed from splash screen", bundle);
    }

    @Override
    protected void onStop() {

        if (!movedToPlayScreen) {
            sendAppClose();
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {

        if (!movedToPlayScreen) {
            sendAppClose();
        }
        super.onDestroy();
    }
}
