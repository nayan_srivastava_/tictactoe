package dns.tictactoe.screens;

import android.content.Context;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Calendar;
import java.util.Random;

import dns.tictactoe.R;
import dns.tictactoe.utils.Constants;
import dns.tictactoe.utils.PrefUtil;

public class PlayScreen extends AppCompatActivity {

    private static final String USER_WINS = "user-counts";
    private static final String BOT_WINS = "bot-counts";
    int c[][];
    int i, j, k = 0;
    Button b[][];
    TextView textView;
    AI ai;

    View reload;
    private static AlphaAnimation anim;
    private boolean gameOver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        //reload = findViewById(R.id.reload);
        AdRequest request = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)        // All emulators
                .addTestDevice("AC98C820A50B4AD8A2106EDE96FB87D4")  // An example device ID
                .build();

        AdView mAdView = (AdView) findViewById(R.id.adView);
        mAdView.loadAd(request);
//        String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
//        String deviceId = (android_id).toUpperCase();
//        AdRequest adRequest = new AdRequest.Builder().addTestDevice(deviceId).build();
//        mAdView.loadAd(adRequest);

        //reload.setOnClickListener(new MyClickListener(0, 0));
        setBoard();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        setBoard();
        return true;
    }

    // Set up the game board.
    private void setBoard() {
        gameOver = false;
        ai = new AI();
        b = new Button[4][4];
        c = new int[4][4];


        textView = (TextView) findViewById(R.id.dialogue);
        textView.setOnClickListener(new MyClickListener(0, 0));

        b[1][3] = (Button) findViewById(R.id.one);
        b[1][2] = (Button) findViewById(R.id.two);
        b[1][1] = (Button) findViewById(R.id.three);


        b[2][3] = (Button) findViewById(R.id.four);
        b[2][2] = (Button) findViewById(R.id.five);
        b[2][1] = (Button) findViewById(R.id.six);


        b[3][3] = (Button) findViewById(R.id.seven);
        b[3][2] = (Button) findViewById(R.id.eight);
        b[3][1] = (Button) findViewById(R.id.nine);

        for (i = 1; i <= 3; i++) {
            for (j = 1; j <= 3; j++)
                c[i][j] = 2;
        }

        textView.setTextColor(getResources().getColor(android.R.color.black));
        textView.setText("Tap any cell to begin");

        // add the click listeners for each button
        for (i = 1; i <= 3; i++) {
            for (j = 1; j <= 3; j++) {
                b[i][j].setOnClickListener(new MyClickListener(i, j));
                b[i][j].setBackgroundColor(getResources().getColor(R.color.cellBackground));
                b[i][j].setText(" ");
                b[i][j].setEnabled(true);
            }
        }
    }


    class MyClickListener implements View.OnClickListener {
        int x;
        int y;


        MyClickListener(int x, int y) {
            this.x = x;
            this.y = y;
        }

        void handleGame() {
            if (b[x][y].isEnabled()) {
                b[x][y].setEnabled(false);
                b[x][y].setText("O");
                b[x][y].setTextColor(getColor(R.color.oColor));
                c[x][y] = 0;
                if (!gameOver) {
                    textView.setText("");
                }
                if (!checkBoard()) {
                    ai.takeTurn();
                }
            }
        }

        public void onClick(View view) {
            if (!gameOver) {
                switch (view.getId()) {
                    case R.id.dialogue:
                        break;
                    default:
                        handleGame();
                        break;
                }
            } else {
                setBoard();
            }
        }
    }


    private class AI {
        void takeTurn() {
            if (c[1][1] == 2 &&
                    ((c[1][2] == 0 && c[1][3] == 0) ||
                            (c[2][2] == 0 && c[3][3] == 0) ||
                            (c[2][1] == 0 && c[3][1] == 0))) {
                markSquare(1, 1);
            } else if (c[1][2] == 2 &&
                    ((c[2][2] == 0 && c[3][2] == 0) ||
                            (c[1][1] == 0 && c[1][3] == 0))) {
                markSquare(1, 2);
            } else if (c[1][3] == 2 &&
                    ((c[1][1] == 0 && c[1][2] == 0) ||
                            (c[3][1] == 0 && c[2][2] == 0) ||
                            (c[2][3] == 0 && c[3][3] == 0))) {
                markSquare(1, 3);
            } else if (c[2][1] == 2 &&
                    ((c[2][2] == 0 && c[2][3] == 0) ||
                            (c[1][1] == 0 && c[3][1] == 0))) {
                markSquare(2, 1);
            } else if (c[2][2] == 2 &&
                    ((c[1][1] == 0 && c[3][3] == 0) ||
                            (c[1][2] == 0 && c[3][2] == 0) ||
                            (c[3][1] == 0 && c[1][3] == 0) ||
                            (c[2][1] == 0 && c[2][3] == 0))) {
                markSquare(2, 2);
            } else if (c[2][3] == 2 &&
                    ((c[2][1] == 0 && c[2][2] == 0) ||
                            (c[1][3] == 0 && c[3][3] == 0))) {
                markSquare(2, 3);
            } else if (c[3][1] == 2 &&
                    ((c[1][1] == 0 && c[2][1] == 0) ||
                            (c[3][2] == 0 && c[3][3] == 0) ||
                            (c[2][2] == 0 && c[1][3] == 0))) {
                markSquare(3, 1);
            } else if (c[3][2] == 2 &&
                    ((c[1][2] == 0 && c[2][2] == 0) ||
                            (c[3][1] == 0 && c[3][3] == 0))) {
                markSquare(3, 2);
            } else if (c[3][3] == 2 &&
                    ((c[1][1] == 0 && c[2][2] == 0) ||
                            (c[1][3] == 0 && c[2][3] == 0) ||
                            (c[3][1] == 0 && c[3][2] == 0))) {
                markSquare(3, 3);
            } else {
                Random rand = new Random();

                int a = rand.nextInt(4);
                int b = rand.nextInt(4);
                while (a == 0 || b == 0 || c[a][b] != 2) {
                    a = rand.nextInt(4);
                    b = rand.nextInt(4);
                }
                markSquare(a, b);
            }
        }


        private void markSquare(int x, int y) {
            b[x][y].setEnabled(false);
            b[x][y].setText("X");
            b[x][y].setTextColor(getColor(R.color.xColor));
            c[x][y] = 1;
            checkBoard();
        }
    }

    private int getRandomIndex(String[] base) {
        long v1 = System.currentTimeMillis() / this.hashCode();
        v1 = v1 ^ getTaskId();
        v1 = (v1 * textView.hashCode()) / ai.hashCode();
        return (int) (v1 % base.length);
    }

    Bundle getFirebaseBundle(String winner, String winKey, int winCount, String dispMessage) {
        Bundle bundle = new Bundle();
        bundle.putInt(winKey, winCount);
        bundle.putString("Winner", winner);
        bundle.putString("at", Calendar.getInstance().getTime().toString());
        bundle.putString("Display message", dispMessage);
        return bundle;
    }

    Animation getAnimation() {
        if (anim == null) {
            anim = new AlphaAnimation(0.8f, 1.0f);
            anim.setDuration(440); //You can manage the blinking time with this parameter
            anim.setStartOffset(40);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
        }
        return anim;
    }

    void blink(boolean won, TextView... tvs) {
        for (TextView tv : tvs) {
            tv.setBackgroundColor(getResources().getColor(!won ? R.color.lostBlinkColor : R.color.winBlink));
            //tt tv.startAnimation(getAnimation());
        }
    }

    boolean markUserVictory(boolean won, int val) {
        boolean res = false;
        if ((c[1][1] == val && c[2][2] == val && c[3][3] == val)) {
            res = true;
            gameOver = true;
            blink(won, b[1][1], b[2][2], b[3][3]);
        } else if ((c[1][3] == val && c[2][2] == val && c[3][1] == val)) {
            blink(won, b[1][3], b[2][2], b[3][1]);
            res = true;
            gameOver = true;
        } else if ((c[1][2] == val && c[2][2] == val && c[3][2] == val)) {
            res = true;
            gameOver = true;
            blink(won, b[1][2], b[2][2], b[3][2]);
        } else if ((c[1][3] == val && c[2][3] == val && c[3][3] == val)) {
            blink(won, b[1][3], b[2][3], b[3][3]);
            res = true;
            gameOver = true;
        } else if ((c[1][1] == val && c[1][2] == val && c[1][3] == val)) {
            blink(won, b[1][1], b[1][2], b[1][3]);
            res = true;
            gameOver = true;
        } else if ((c[2][1] == val && c[2][2] == val && c[2][3] == val)) {
            blink(won, b[2][1], b[2][2], b[2][3]);
            res = true;
            gameOver = true;
        } else if ((c[3][1] == val && c[3][2] == val && c[3][3] == val)) {
            blink(won, b[3][1], b[3][2], b[3][3]);
            res = true;
            gameOver = true;
        } else if ((c[1][1] == val && c[2][1] == val && c[3][1] == val)) {
            blink(won, b[1][1], b[2][1], b[3][1]);
            res = true;
            gameOver = true;
        }

        return res;
    }

    void gameOver() {
        if (gameOver) {
            for (i = 1; i <= 3; i++) {
                for (j = 1; j <= 3; j++) {
                    b[i][j].setEnabled(true);
                }
            }
        }
    }

    private boolean checkBoard() {
        if (!gameOver) {
            if (markUserVictory(true, 0)) {
                int num = PrefUtil.getInt(USER_WINS, this) + 1;
                PrefUtil.putInt(USER_WINS, num, this);
                int index = getRandomIndex(Constants.USER_WIN_MESSAGE);
                textView.setText(Constants.USER_WIN_MESSAGE[index]);
                textView.setTextColor(getResources().getColor(R.color.victoyTextColor));
                FirebaseAnalytics.getInstance(this).logEvent("Result", getFirebaseBundle("user", USER_WINS, num, Constants.USER_WIN_MESSAGE[index]));
                gameOver();
            } else if (markUserVictory(false, 1)) {
                int num = PrefUtil.getInt(BOT_WINS, this) + 1;
                PrefUtil.putInt(BOT_WINS, num, this);
                int index = getRandomIndex(Constants.BOT_WIN_MESSAGES);
                textView.setText(Constants.BOT_WIN_MESSAGES[index]);
                FirebaseAnalytics.getInstance(this).logEvent("Result", getFirebaseBundle("bot", BOT_WINS, num, Constants.BOT_WIN_MESSAGES[index]));
                textView.setTextColor(getResources().getColor(R.color.lostTestColor));
                gameOver();
            } else {
                boolean empty = false;
                for (i = 1; i <= 3; i++) {
                    for (j = 1; j <= 3; j++) {
                        if (c[i][j] == 2) {
                            empty = true;
                            break;
                        }
                    }
                }
                if (!empty) {
                    gameOver = true;
                    gameOver();
                    textView.setTextColor(getResources().getColor(R.color.drawColor));
                    textView.setText("Game over. It's a draw!");
                }
            }
        }
        return gameOver;
    }
}


